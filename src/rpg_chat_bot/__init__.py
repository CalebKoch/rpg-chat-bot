import sys

from . import commands


def search():
	search_term = sys.argv[-1]
	search_result = commands.search(search_term)

	return(commands.format_search_result(search_result))


if (__name__ == "__main__"):
	search_result = search()

	print(search_result)
