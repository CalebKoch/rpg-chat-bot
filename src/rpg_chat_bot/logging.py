import logging
import os

def init_logging():
	LOG_LEVEL = os.getenv("LOG_LEVEL")
	LOG_LEVELS = {
			'DEBUG': logging.DEBUG,
			'INFO': logging.INFO,
			'WARN': logging.WARN,
		}
	
	if LOG_LEVEL in LOG_LEVELS:
		logging.basicConfig(
			level=LOG_LEVELS[LOG_LEVEL]
		)
	else:
		logging.basicConfig()

