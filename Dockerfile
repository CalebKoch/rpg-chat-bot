FROM python:3.12-alpine

COPY ./requirements* /app/
WORKDIR /app

RUN pip install -r requirements.txt
COPY . .
RUN pip install .

ENTRYPOINT ["python", "main.py"]

