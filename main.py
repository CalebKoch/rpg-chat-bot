import sys

def main():
	if sys.argv[-1] == "matrix":
		import rpg_chat_bot.matrix
		rpg_chat_bot.matrix.start()
	elif sys.argv[-1] == "discord":
		import rpg_chat_bot.discord
		rpg_chat_bot.discord.start()
	else:
		print("Please specify the chat service.")

if __name__ == "__main__":
	main()
